﻿using System;

namespace Estrutura_Repetitiva
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Digite a Chave de Segurança: ");
            int chave = int.Parse(Console.ReadLine());

            while (chave != 2811)
            {
                Console.WriteLine("Chave de segurança INCORRETA:");
                chave = int.Parse(Console.ReadLine());
            }
            Console.WriteLine("ACESSO PERMITIDO");
        }
    }
}
