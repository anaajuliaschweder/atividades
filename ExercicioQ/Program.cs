﻿using System;

namespace ExercicioQ
{
    class Program
    {
        static void Main(string[] args)
        {
            //EXEMPLOS DE OPERADORES COMPARATRIVOS!
            int a = 10;
            bool c1 = a < 10;
            bool c2 = a < 20;
            bool c3 = a > 10;
            bool c4 = a > 5;

            Console.WriteLine(c1);
            Console.WriteLine(c2);
            Console.WriteLine(c3);
            Console.WriteLine(c4);

            //EXEMPLOS DE OPERADORES LÓGICOS
            bool c5 = 2 > 3 || 4 != 5;

            Console.WriteLine(c5);
        }
    }
}
