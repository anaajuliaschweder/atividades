﻿using System;

namespace ExercicioN
{
   public class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Digite sua chave de segurança:");

            //int dado primitivo(tipo da variável)
            int chave = int.Parse(Console.ReadLine());

            //!= diferente
            while (chave != 2811) 
            {
                Console.WriteLine("Chave de segurança inválida");
                chave = int.Parse(Console.ReadLine());
            }
            Console.WriteLine("Acesso Permitido!! ");
        }
    }
}
