﻿using System;
using System.Globalization;

namespace ExercicioC
{
    public class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Digite o valor: ");

            double C, Area, pi = 6.65321;
            C = double.Parse(Console.ReadLine(), CultureInfo.InvariantCulture);
            // o valor (C) é multiplicado pelo PI.
            Area = pi * C ;

            Console.WriteLine("Area = " + Area.ToString("F3", CultureInfo.InvariantCulture));
        }
    }
}
