﻿using System;
using System.Globalization;

namespace ExercicioL
{
    public class Program
    {
        static void Main(string[] args)
        {
            //Primeiro valor Y e segundo valor X
            Console.WriteLine("Digite dois valores: ");
            string[] valor = Console.ReadLine().Split(' ');
            double x = double.Parse(valor[0], CultureInfo.InvariantCulture);
            double y = double.Parse(valor[1], CultureInfo.InvariantCulture);

            //== igual
            if (x == 0.0 && y == 0.0) 
            {
                Console.WriteLine("Origem");
            }

            else if (x == 0.0) 
            {
                Console.WriteLine("Pertence ao eixo Y");
            }

            else if (y == 0.0) 
            {
                Console.WriteLine("Pertence ao eixo X");
            }

            //2,1
            else if (x > 0.0 && y > 0.0) 
            {
                Console.WriteLine("Q1");
            }

            //-3,2
            else if (x < 0.0 && y > 0.0) 
            {
                Console.WriteLine("Q2");
            }
            //-2,-2
            else if (x < 0.0 && y < 0.0)
            {
                Console.WriteLine("Q3");
            }
            //3,-3
            else 
            {
                Console.WriteLine("Q4");
            }
        }
    }
}
