﻿using System;

namespace ExercicioB
{
    public class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Digite os valores para ter o resultado:");

            int A, J, resultado;
            A = int.Parse(Console.ReadLine());
            J = int.Parse(Console.ReadLine());

            resultado = A + J;

            Console.WriteLine("O resultado é: " + resultado);
        }
    }
}
