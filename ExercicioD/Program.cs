﻿using System;

namespace ExercicioD
{
    public class Program
    {
        static void Main(string[] args)
        {
            int a, e, i, o, diferente;
            a = int.Parse(Console.ReadLine());
            e = int.Parse(Console.ReadLine());
            i = int.Parse(Console.ReadLine());
            o = int.Parse(Console.ReadLine());

            diferente = a * e - i * o;
            Console.WriteLine("Diferença: " + diferente);
        }
    }
}
