﻿using System;

namespace ExercicioH
{
    public class Program
    {
        static void Main(string[] args)
        {
            string[] valor = Console.ReadLine().Split(' ');
            int A = int.Parse(valor[0]);
            int B = int.Parse(valor[1]);

            //Se a sobra de A e B for igual a 0 OU se a sobra de B e A for igual a 0 eles serão números multiplos
            if (A % B == 0 || B % A == 0) 
            {
                Console.WriteLine("Esses números são múltiplos");
            }
                       
            // se não
            else 
            {
                Console.WriteLine("Esses números não são múltiplos");
            }
        }
    }
}
