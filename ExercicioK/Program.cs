﻿using System;
using System.Globalization;

namespace ExercicioK
{
    public class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Digite um valor real: ");

            double numero = double.Parse(Console
            .ReadLine(), CultureInfo.InvariantCulture);

            if (numero < 0.0 || numero > 100.0) 
            {
                Console.WriteLine("Incorreto");
            }

            else if (numero <= 25.0) 
            {
                Console.WriteLine("O intervalo é (0,25)");
            }

            else if (numero <= 50.0) 
            {
                Console.WriteLine("O intervalo é (25,50)");
            }

            else if (numero <= 75.0) 
            {
                Console.WriteLine("O intervalo é (50,75)");
            }

            else 
            {
                Console.WriteLine("O intervalo é (75,100)");
            }
        }
    }
}
