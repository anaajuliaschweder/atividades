﻿using System;

namespace ExercicioP
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Digite o código do produto de sua preferencia: ");

            int produto = int.Parse(Console.ReadLine());
            int gasolina = 0;
            int alcool = 0;
            int diesel = 0;

            while (produto != 4) {
                if (produto == 1)
                {
                    gasolina++;
                    Console.WriteLine("Gasolina");
                }

                else if (produto == 2) {
                    alcool++;
                    Console.WriteLine("Alcool");
                }

                else if (produto == 3) {
                    diesel++;
                    Console.WriteLine("Diesel");
                }

                produto = int.Parse(Console.ReadLine());
            }
            Console.WriteLine("OBRIGADO PELA PREFERÊNCIA");
            Console.WriteLine("*************************************************");
            Console.WriteLine("Quantidade de clientes que abasteceram Gasolina : " + gasolina);
            Console.WriteLine("Quantidade de clientes que abasteceram Alcool: " + alcool);
            Console.WriteLine("Quantidade de clientes que abasteceram Diesel: " + diesel);
        }
    }
}
