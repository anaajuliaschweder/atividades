﻿using System;

namespace Estrutura_Condicional
{
    class Program
    {
        static void Main(string[] args)
        {
            int N = int.Parse(Console.ReadLine());

            if (N < 0)
            {
                Console.WriteLine("Este número é negativo");
            }
            else
            {
                Console.WriteLine("Este número é positivo");
            }
        }
    }
}
