﻿using System;

namespace ExercicioR
{
    class Program
    {
        static void Main(string[] args)
        {
            Pessoa n1 = new Pessoa();
            Pessoa n2 = new Pessoa();

            Console.WriteLine("DIGITE SEUS DADOS ABAIXO ");
            Console.WriteLine("**********************************");
            Console.Write("Nome: ");
            n1.Nome = Console.ReadLine();
            Console.Write("Idade: ");
            n1.Idade = int.Parse(Console.ReadLine());

            Console.WriteLine("**********************************");
            Console.WriteLine("DIGITE SEUS DADOS ABAIXO ");
            Console.Write("Nome: ");
            n2.Nome = Console.ReadLine();
            Console.Write("Idade: ");
            n2.Idade = int.Parse(Console.ReadLine());
            Console.WriteLine("**********************************");

            if (n1.Idade > n2.Idade)
            {
                Console.WriteLine("Pessoa mais velha: " + n1.Nome);
            }
            else
            {
                Console.WriteLine("Pessoa mais velha: " + n2.Nome);
            }
        }
    }
}
