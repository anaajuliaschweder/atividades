﻿namespace ExercicioT
{
    internal class Funcionario
    {
        public string Nome { get; internal set; }
        public double Salario { get; internal set; }
    }
}