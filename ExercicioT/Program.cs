﻿using System;
using System.Globalization;

namespace ExercicioT
{
    class Program
    {
        static void Main(string[] args)
        {
            Funcionario n1 = new Funcionario();
            Funcionario n2 = new Funcionario();

            Console.WriteLine("DIGITE SEUS DADOS ABAIXO ");
            Console.Write("Nome: ");
            n1.Nome = Console.ReadLine();
            Console.Write("Salario: ");
            n1.Salario = double.Parse(Console.ReadLine(), CultureInfo.InvariantCulture);

            Console.WriteLine("**********************************");
            Console.WriteLine("DIGITE SEUS DADOS ABAIXO ");
            Console.Write("Nome: ");
            n2.Nome = Console.ReadLine();
            Console.Write("Salario: ");
            n2.Salario = double.Parse(Console.ReadLine(), CultureInfo.InvariantCulture);

            double media = (n1.Salario + n2.Salario) / 2.0;

            Console.WriteLine("O salário médio é: " + media.ToString("F2", CultureInfo.InvariantCulture));
        }
    }
}
