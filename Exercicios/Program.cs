﻿using System;
using System.Globalization;

namespace Exercicios
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("1-Digite o código do funcionário");
            Console.WriteLine("2-Digite o número de horas trabalhadas");
            Console.WriteLine("3-Digite o valor recebido por hora");
            Console.WriteLine("------------------------------------------");

            int numeroFuncionario, horasTrabalhadas;
            double valorPorHora, calculo;

            numeroFuncionario = int.Parse(Console.ReadLine());
            horasTrabalhadas = int.Parse(Console.ReadLine());
            valorPorHora = double.Parse(Console.ReadLine(), CultureInfo.InvariantCulture);

            calculo = valorPorHora * horasTrabalhadas;

            Console.WriteLine("------------------------------------------");
            Console.WriteLine("Funcionário de código: " + numeroFuncionario);
            Console.WriteLine("Recebe o valor de: " + calculo.ToString("F2", CultureInfo.InvariantCulture));
        }
    }
}
