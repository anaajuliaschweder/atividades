﻿using System;

namespace ExercicioI
{
    public class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Horário do inicio e fim do jogo: ");
            string[] valor = Console.ReadLine().Split(' ');

            int horaInicial = int.Parse(valor[0]);
            int horaFim = int.Parse(valor[1]);
            int duracao;

            if(horaInicial < horaFim) 
            {
                duracao = horaFim - horaInicial;
            }

            else 
            {
                duracao = 24 - horaInicial + horaFim;
            }
            Console.WriteLine("O jogo teve a durãção de " + duracao + " horas");
        }
    }
}
