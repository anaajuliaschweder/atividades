﻿using System;
using System.Globalization;

namespace ExercicioJ
{
   public class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Digite o id e a quantidade dos itens escolhidos separando-os por uma vírgula: ");
            string[] valor = Console.ReadLine().Split(',');
            int id = int.Parse(valor[0]);
            int quantidade = int.Parse(valor[1]);
            double total;

            if (id == 1) 
            {
                total = quantidade * 4.0;
            }

            else if (id == 2) 
            {
                total = quantidade * 4.5;
            }

            else if (id == 3) 
            {
                total = quantidade * 5.0;
            }

            else if (id == 4) 
            {
                total = quantidade * 2.0;
            }

            else 
            {
                total = quantidade * 1.5;
            }
            Console.WriteLine("Total: " + total.ToString("F3", CultureInfo.InvariantCulture));
        }
    }
}
