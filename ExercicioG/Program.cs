﻿using System;

namespace ExercicioG
{
   public class Program
    {
        static void Main(string[] args)
        {
            // % resto da soma
            Console.WriteLine("Digite um número: ");

            int Numero = int.Parse(Console.ReadLine());
            // se a sobra dos dois forem 0 entra nacondição do if
            if (Numero % 2 == 0) 
            {
                Console.WriteLine("Número Par");
            }
            //se não
            else 
            {
                Console.WriteLine("Número Impar");
            }
        }
    }
}
