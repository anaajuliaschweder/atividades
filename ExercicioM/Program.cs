﻿using System;
using System.Globalization;

namespace ExercicioM
{
   public class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Digite sua renda para saber o valor do imposto");
            double renda = double.Parse(Console.ReadLine(), CultureInfo.InvariantCulture);

            double imposto;
            if (renda <= 5000.0) 
            {
                imposto = 0.0;
            }

            else if (renda <= 7000.0) 
            {
                imposto = (renda - 5000.0) * 0.08;
            }

            else if (renda <= 3500.0) 
            {
                imposto = (renda - 7000.0) * 0.18 + 1000.0 * 0.08;
            }

            else 
            {
                imposto = (renda - 3500.0) * 0.28 + 1500.0 * 0.18 + 1000.0 * 0.08;
            }

            if (imposto == 0.0) 
            {
                Console.WriteLine("Voce é isento");
            }

            else 
            {
                Console.WriteLine("R$ " + imposto.ToString("F3", CultureInfo.InvariantCulture));
            }
        }
    }
}
