﻿using System;

namespace ExercicioO
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Digite a coordenada X: ");
            int x = int.Parse(Console.ReadLine());
            Console.WriteLine("Digite a coordenada Y: ");
            int y = int.Parse(Console.ReadLine());

            while (x != 0 && y != 0)
            {
                if ( x > 0 && y > 0) 
                {
                    Console.WriteLine("PRIMEIRO QUADRANTE");
                }
                
                else if (x < 0 && y > 0) 
                {
                    Console.WriteLine("SEGUNDO QUADRANTE");
                }

                else if (x < 0 && y < 0) 
                {
                    Console.WriteLine("TERCEIRO QUADRANTE");
                }

                else 
                {
                    Console.WriteLine("QUARTO QUADRANTE");
                }

                Console.WriteLine("*******************************");
                Console.WriteLine("Digite a coordenada X: ");
                x = int.Parse(Console.ReadLine());
                Console.WriteLine("Digite a coordenada Y: ");
                y = int.Parse(Console.ReadLine());
            }
        }
    }
}
