﻿using System;

namespace Estrutura_Sequencial
{
    class Atividade2
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Digite os valores para ter o resultado:");

            int A, J, resultado;

            A = int.Parse(Console.ReadLine());
            J = int.Parse(Console.ReadLine());

            resultado = A + J;

            Console.WriteLine("O resultado é: " + resultado);
        }
    }
}
