﻿using System;
using System.Globalization;

namespace Estrutura_Sequencial
{
    class Atividade3
    {
        static void Main(string[] args)
        {
            double C, A, pi = 6.65321;

            C = double.Parse(Console.ReadLine(), CultureInfo.InvariantCulture);

            A = pi * C * C;

            Console.WriteLine("A=" + A.ToString("F4", CultureInfo.InvariantCulture));
        }
    }
}
